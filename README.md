# USAGE

### Preparing environment

Within tox.ini folder, use following command:
`tox -e test``

After command succeed, use following commands to generate history:

**Windows**

.\.tox\IRCDump\Scripts\IRCDump.exe -l https://m.twitch.tv/mrsavage/videos -o output.log -c <secret_client_id>
W: Attempt 1 connection to https://www.twitch.tv/videos/1989565774 to achieve video title. 
Success: title for this video found - CARRYING MONGRAAL AND UNKNOWN IN UNREAL newvid mrsavage on Twitch
Dumping history in progress.. Saving to path output.log. 


**Unix**

.\.tox\IRCDump\bin\IRCDump -l https://m.twitch.tv/mrsavage/videos -o output.log -c <secret_client_id>
W: Attempt 1 connection to https://www.twitch.tv/videos/1989565774 to achieve video title. 
Success: title for this video found - CARRYING MONGRAAL AND UNKNOWN IN UNREAL newvid mrsavage on Twitch
Dumping history in progress.. Saving to path output.log. 
