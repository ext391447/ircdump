import pytest


@pytest.mark.parametrize("input, expected", [(1, 1), (2, 2)])
def test_notest(input, expected):
    assert input == expected
