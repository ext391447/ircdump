from IRCDump import __main__


def test_main(mocker):
    main_mock = mocker.MagicMock()
    mocker.patch("IRCDump.__main__.main", side_effect=main_mock)
    __main__.main(["-l", "https://www.twitch.tv/userxyz/videos", "-o", "undefined"])
    main_mock.assert_called_once()
