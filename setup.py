from setuptools import setup, find_packages


def run_setup():
    setup(
        name="IRCDump",
        version="0.0.1",
        description="IRC based Twitch history dumping tool. ",
        author="WN",
        author_email="wojciechnowit@gmail.com",
        url="https://example.com",
        packages=find_packages(
            where="src",
            exclude=[
                "tests",
                "docs",
                ".gitignore",
                "README.MD",
                "*.txt",
                "*.pyc",
                "main.py",
            ],
        ),
        package_dir={"": "src"},
        lint_requires=["flake8"],
        install_requires=["requests", "bs4"],
        extras_require={
            "black": ["black~=23.0"],
            "lint": ["flake8"],
            "test": ["pytest", "pytest-timeout", "pytest-mock"],
        },
        entry_points={"console_scripts": ["IRCDump = IRCDump.__main__:main"]},
    )


if __name__ == "__main__":
    run_setup()
