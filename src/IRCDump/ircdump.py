import requests
import re
import time
from string import ascii_letters
import json
from IRCDump.configuration import GQL_URL, GQL_REQUEST


class IRCDump:
    def __init__(
        self, url: str, client_id: str, output_log: str = "history.log"
    ) -> None:
        self._time_limit: int = 60
        self._current_state: int = 0  # 21_200
        self._boundary_test: int = 0xFFFFFFFFF
        self._path: str = output_log
        self._sleep_duration = 0.5
        self._video_id: str = ""

        self._session = requests.Session()
        self.client_id = client_id
        self._data: bytes = b""
        self._re_pattern = re.compile(r"\.(\d*)")
        self.url = self.validate_url(url)
        self.real_url = self.combine_real_url(self.url)
        self._video_title: str = self.make_title_request()
        self._boundary: int = self.make_boundary_request()

        self._history_with_names = {}
        # OPEN IDEAS
        # self.json_container: json.JSONEncoder = json # TODO: use this

    def make_title_request(self):
        # TODO: fix title pattern, refactor loops statments
        # TODO: add: logging instead of printing to standard output
        # pattern = re.compile('<meta name="title" content="(\w+ \w+)+')
        pattern = re.compile('<meta name="title" content="(.*?)"+')

        for i in range(1, 5 + 1):
            request = self._session.get(self.url)
            title = re.findall(pattern, request.text)
            print(f"W: Attempt {i} connection to {self.url} to achieve video title. ")
            time.sleep(1)

            if title:
                break

        if not title:
            print(
                "Warning: title for this video not found"
                " - using video id for logging name instead. {}".format(self._video_id)
            )
            return self._video_id

        title = "".join(
            list(filter(lambda x: x in ascii_letters + " ", title[0]))
        ).strip()
        title = re.sub(
            "([ ]+)", " ", title
        )  # redundant spaces removal, maybe fix due to complexity reason
        assert title, title

        print("Success: title for this video found - {}".format(title))
        return title

    def make_latest_videos_request(self):
        """
        Still in TODO:
        Implementation steps:
                1. 'https://m.twitch.tv/{user}}/videos'
                2. Afore mentioned url provides "sometimes"
                        url to for the latest videos.
                3. Find out which order of the videos are delivered.
                4. Make that automated.
        """
        pass

    def make_boundary_request(self):
        url = GQL_URL
        data = GQL_REQUEST
        data[0]["variables"]["videoID"] = self._video_id  # TODO: make it more fancy
        data[0]["variables"][
            "contentOffsetSeconds"
        ] = 0  # TODO: remove, just for testing

        boundary_request = self._session.post(
            url, headers={"CLIENT-ID": self.client_id}, data=json.dumps(data)
        )
        boundary_data = json.loads(boundary_request.text)
        offset = boundary_data[0]["extensions"]["durationMilliseconds"]
        return (
            int(offset) + 0xFFFFFFFFFFF
        )  # TODO: return time with video duration (in seconds)

    def _chat_history_request(self):
        url = GQL_URL
        data = GQL_REQUEST
        # TODO: make it more fancy
        data[0]["variables"]["videoID"] = self._video_id
        data[0]["variables"]["contentOffsetSeconds"] = self._current_state
        # TODO: if sended as data[0]
        #  then in return we dont need to use again [0] in base_messages
        data = json.dumps(data)

        try:
            request = self._session.post(
                url, headers={"CLIENT-ID": self.client_id}, data=data
            )
        except Exception as e:
            print(f"Error: {e.message}")
            # TODO: better handling?
        self._current_state += self._time_limit
        boundary_data = json.loads(request.text)

        if boundary_data[0].get("errors"):
            print(
                "Warning: found that boundary was wrong and received end of transsmison. "
            )
            self._current_state = 0xFFFFFFFFFFFFFFFFFFFFFFF
            return self.history_dump

        base_messages = [
            (
                x["node"]["message"]["fragments"][0]["text"]
                if x["node"]["message"]["fragments"]
                else "NO_MESSAGE",
                x["node"]["commenter"]["displayName"]
                if x["node"]["commenter"] is not None
                else "NO_COMMENTER",
            )
            for x in boundary_data[0]["data"]["video"]["comments"]["edges"]
        ]

        for item in base_messages:
            commenter_message, commenter_name = item

            if self._history_with_names.get(commenter_name, None) is None:
                self._history_with_names[commenter_name] = []

            # TODO: IMPORTANT REMOVE THIS AFTER KNOWING
            # TIME-SPAN FOR UPDATING MESSAGES
            if commenter_message not in self._history_with_names[commenter_name]:
                self._history_with_names[commenter_name].append(commenter_message)

        return self.history_dump  # propably its bad

    @property
    def history_dump(self):
        return self._history_with_names

    def _dump_history(self, debug: bool = False):
        while self._current_state < self._boundary:
            print(
                "[?] Request is done with url {URL}. History for timespan {TIMESPAN}. ".format(
                    URL=self.url, TIMESPAN=self._current_state
                )
            )
            self._chat_history_request()
            time.sleep(self._sleep_duration)

        # maybe logging should be before while loop?
        if debug:
            print("Success: saving log into {}. ".format(self._path))
            self.save_log()

    def dump_history(self):
        print("Dumping history in progress.. Saving to path {}. ".format(self._path))
        self._dump_history(debug=True)
        print("Dumping history finshed. ")

    def combine_real_url(self, url):
        # TODO: commonize
        prefix = "https://api.twitch.tv/v5/videos/"
        suffix = "/comments?content_offset_seconds="

        url_video_id = url.split("/")[-1]
        self._video_id = url_video_id
        prepared_url = f"{prefix}{url_video_id}{suffix}"
        return prepared_url

    def validate_url(self, url):
        assert isinstance(url, str), url
        return url

    def request_for_history(self):
        self._session.get(self.url, headers={})

    @property
    def data(self):
        return self._data

    def save_log(self):
        real_name = self._path.split(".")
        assert real_name[-1] == "log"

        with open(
            "{}{}.log".format(real_name[0], self._video_title), "w", encoding="utf-8"
        ) as f:
            assert isinstance(self.history_dump, dict)
            dump_data = ""
            for name, messages in self.history_dump.items():
                # TODO: maybe another fucntion
                # like parse_data to log?
                message_data = "\n".join(messages)
                dump_data += "\n{name}:\n".format(name=name)
                dump_data += message_data

            f.write(dump_data)

    @property
    def boundary(self):
        return self._boundary
