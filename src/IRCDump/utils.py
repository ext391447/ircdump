import re
from functools import partial
from time import sleep
from typing import Optional, Type, Union

import requests


class AttemptedRequest(object):
    def __init__(self, timeout: int = 1, retries: int = 3, func: Optional[Type] = None):
        self._timeout = timeout
        self._retries = retries
        self._func = func
        self.validate_args()  # TODO: refactor
        self._responses = []
        self._returned_data = None

    def validate_args(self) -> bool:
        assert isinstance(self._timeout, int), "Timeout must be a int type. "
        assert isinstance(self._retries, int), "Retries must be a int type. "

    def __enter__(self):
        for _ in range(0, self._retries):
            sleep(self._timeout)
            result, response = self._func()
            self._responses.append(response)  # TODO: is it really needed?

            if result:
                break

        return result

    def __exit__(self, *args, **kwargs):
        if any(args):
            raise Exception(f"Please handle following exceptions: {args}")  # refactor


def request_with_pattern(url: str, pattern: str) -> Union[str, None]:
    # wn: refactor, so that parameters could be verified `on the fly`.
    assert isinstance(url, str), "Variable `url` must by a string type. "
    assert isinstance(pattern, str), "Variable `pattern` must by a string type. "

    request = requests.get(url, allow_redirects=True)
    data = request.text
    found = re.findall(pattern, data)

    return (found or None, request)


def get_latest_videos(profile_url: str, scope: int = 10) -> list:
    # Returns a list with videos links in a ASC order (newest to the oldest).
    pattern = r"url\":\"https://www.twitch.tv/videos/(\d+)"  # move it to utils.pattern
    with AttemptedRequest(
        timeout=1, retries=3, func=partial(request_with_pattern, profile_url, pattern)
    ) as ar_data:
        allowed_format_list = [
            f"https://www.twitch.tv/videos/{video_id}" for video_id in ar_data
        ]

    return allowed_format_list or None
