import sys
import argparse
from IRCDump.utils import get_latest_videos
from IRCDump.ircdump import IRCDump
from time import time as builtin_time


def parse_args(args) -> argparse.Namespace:
    long_desc = """
                -l, --link: specify link to the target video hosted by Twitch e.g.
                https://m.twitch.tv/{user}/videos,
                -o, --output: specify path where output data will be saved,
                -c, --client-id: user auth token.
                """
    parser = argparse.ArgumentParser(
        "IRCDump", description=long_desc, formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument("-l", "--link", action="store", type=str, required=True)
    parser.add_argument("-o", "--output", action="store", type=str, required=True)
    parser.add_argument("-c", "--client-id", action="store", type=str, required=True)
    return parser.parse_args(args)


def main(args=sys.argv[1:]):
    args = parse_args(args)
    start = builtin_time()

    while True:
        try:
            next(
                IRCDump(
                    url, client_id=args.client_id, output_log=args.output
                ).dump_history()
                for url in get_latest_videos(args.link)
            )
        except StopIteration:
            print(f"IRCDump: Finished in {(builtin_time() - start):.2f}!")
            break


if __name__ == "__main__":
    main()
