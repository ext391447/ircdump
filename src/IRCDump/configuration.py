import json

GQL_URL = "https://gql.twitch.tv/gql"
GQL_REQUEST = json.loads(
    """[
        {
        "operationName": "VideoCommentsByOffsetOrCursor",
        "variables": {
            "videoID": "1719035694",
            "contentOffsetSeconds": 0
        },
        "extensions": {
            "persistedQuery": {
                "version": 1,
                "sha256Hash": "b70a3591ff0f4e0313d126c6a1502d79a1c02baebb288227c582044aa76adf6a"
            }
        }
    }
]"""
)
